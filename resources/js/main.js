import Vue from 'vue'
import Router from 'vue-router'

import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.css'

import Vuex from 'vuex'

Vue.use(Vuex)

// Create a new store instance.
const store = new Vuex.Store({
  state () {
    return {
      authToken: null
    }
  },
  mutations: {
    setAuthToken(state, token) {
      state.authToken = token;
    }
  }
})


Vue.use({
  install (Vue) {
    Vue.prototype.$api = axios.create({
      baseURL: 'https://php-mvc.test/api/',
      headers: {'Authorization': store.state.authToken}
    })
  }
})

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';

Vue.component('v-select', vSelect)


import App from './App.vue'
import Login from './components/Login.vue'
import Catalog from './components/Catalog.vue'

import ProductsList from './components/admin/product/List.vue'
import ProductsCreate from './components/admin/product/Create.vue'
import ProductsUpdate from './components/admin/product/Update.vue'

// errors
import Error404 from './components/errors/Error404Component.vue';
Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: "/",
  routes: [
    {
      path: '/',
      name:'catalog',
      component: Catalog,
    },
    {
      path: '/login',
      name:'login',
      component: Login,
    },
    {
      path: '/admin/products',
      name:'admin.products.list',
      component: ProductsList,
    },
    {
      path: '/admin/products/create',
      name:'admin.products.create',
      component: ProductsCreate,
    },
    {
      path: '/admin/products/:id/update',
      name:'admin.products.update',
      component: ProductsUpdate,
      props: true,
    },
    {
      path: '/*',
      name: 'Error404',
      component: Error404
    },
  ]
})

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
});