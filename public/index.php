<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('api/login', [
    'method' => 'post',
    'namespace' => 'Api',
    'controller' => 'Auth',
    'action' => 'login',
]);

$router->add('api/products', [
    'method' => 'post',
    'namespace' => 'Api',
    'controller' => 'Catalog',
    'action' => 'index',
]);

$router->add('api/admin/storage_locations', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'StorageLocation',
    'action' => 'index',
]);
$router->add('api/admin/categories', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'Category',
    'action' => 'index',
]);
$router->add('api/admin/products', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'Product',
    'action' => 'index',
]);
$router->add('api/admin/products/create', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'Product',
    'action' => 'create',
]);
$router->add('api/admin/products/{id:\d+}/read', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'Product',
    'action' => 'read',
]);
$router->add('api/admin/products/{id:\d+}/update', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'Product',
    'action' => 'update',
]);
$router->add('api/admin/products/{id:\d+}/delete', [
    'method' => 'post',
    'namespace' => 'Api\Admin',
    'controller' => 'Product',
    'action' => 'delete',
]);

$router->add('{any:.*}', [
    'method' => 'get',
    'controller' => 'Spa',
    'action' => 'index'
]);
// $router->add('{controller}/{action}');
    
$router->dispatch($_SERVER['QUERY_STRING']);
