<?php

namespace App\Models;

use PDO;
use Storage;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Product extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM products");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // public static function getAllIds()
    // {
    //     $db = static::getDB();
    //     $stmt = $db->query("SELECT id FROM products");
    //     return $stmt->fetchAll(PDO::FETCH_COLUMN);
    // }

    public static function findByTitle($title)
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM products WHERE title = '$title'");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function findById($id)
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM products WHERE id = '$id'");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function findByIdWithRelated($id){
        $product = self::findById($id);
        if(!$product)
            return null;

        $db = static::getDB();
        $stmt = $db->query("
            SELECT `storage_locations`.*, `product_storage_location`.`product_id` as `pivot_product_id`, `product_storage_location`.`storage_location_id` as `pivot_storage_location_id` 
                FROM `storage_locations` 
                    inner join `product_storage_location` 
                    on `storage_locations`.`id` = `product_storage_location`.`storage_location_id` 
                where `product_storage_location`.`product_id` = $id
        ");
        $storage_locations = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $product['storage_locations'] = $storage_locations;
        $product['category'] = Category::findById($product['category_id']);
        return $product;
    }

    public static function update($id, $data)
    {
        $preparedData['title'] = array_key_exists('title', $data) ? $data['title'] : null;
        $preparedData['category_id'] = array_key_exists('category_id', $data) ? $data['category_id'] : null;
        
        $db = static::getDB();
        $stmt = $db->prepare("UPDATE `products` SET title=:title, category_id=:category_id  WHERE id = $id");
        $stmt->execute($preparedData);

        $storage_location_ids = $data['storage_location_ids'];
        self::syncStorageLocation($id, $storage_location_ids);
        $stmt = $db->query("SELECT * FROM products WHERE id = $id");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function create($data)
    {
        $preparedData['title'] = array_key_exists('title', $data) ? $data['title'] : null;
        $preparedData['category_id'] = array_key_exists('category_id', $data) ? $data['category_id'] : null;

        $db = static::getDB();
        $stmt = $db->prepare("INSERT INTO `products` (`id`, `title`, `category_id`) VALUES (NULL, :title, :category_id)");
        $stmt->execute($preparedData);
        $lastInsertedId = $db->lastInsertId();

        $storage_location_ids = $data['storage_location_ids'];
        self::syncStorageLocation($lastInsertedId, $storage_location_ids);
        $stmt = $db->query("SELECT * FROM products WHERE id = $lastInsertedId");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function delete($ids)
    {
        if(is_array($ids)){
            foreach($ids as $id){
                self::syncStorageLocation($id, []);
            }
            $ids = implode(', ', $ids);
        }
        else{
            self::syncStorageLocation($ids, []);
        }

        $db = static::getDB();
        $stmt = $db->query("DELETE FROM products WHERE id in ($ids)");
        return $stmt->execute();
    }

    public static function getAllWithRelated(){
        $products = self::getAll();
        if(!count($products))
            return [];
        $ids = array_map(function ($product){
            return $product['id'];
        }, $products);
        $ids_string = implode(', ', $ids);

        $db = static::getDB();
        $stmt = $db->query("
            SELECT `storage_locations`.*, `product_storage_location`.`product_id` as `pivot_product_id`, `product_storage_location`.`storage_location_id` as `pivot_storage_location_id` 
                FROM `storage_locations` 
                    inner join `product_storage_location` 
                    on `storage_locations`.`id` = `product_storage_location`.`storage_location_id` 
                where `product_storage_location`.`product_id` in ($ids_string)
        ");
        $storage_locations = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $products = array_map(function($product) use ($storage_locations){
            $product['storage_locations'] = array_filter($storage_locations, function($sl) use ($product){
                return $sl['pivot_product_id'] == $product['id'];
            });
            return $product;
        }, $products);
        $categories = Category::getAll();
        $products = array_map(function($product) use ($categories){
            $product['category'] = $categories[array_search($product['category_id'], array_column($categories, 'id'))];
            return $product;
        }, $products);
        return $products;
    }

    public static function syncStorageLocation($product_id, $storage_location_ids){
        $db = static::getDB();
        $stmt = $db->query("SELECT id FROM product_storage_location WHERE product_id = '$product_id'");
        $pivot = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($pivot){
            $pivot_ids = array_map(function ($pivot){
                return $pivot['id'];
            }, $pivot);
            $pivot_ids_string = implode(', ', $pivot_ids);

            $stmt = $db->query("DELETE FROM product_storage_location WHERE id in ($pivot_ids_string)");
            $stmt->execute();
        }

        foreach($storage_location_ids as $sl_id){
            $stmt = $db->prepare("INSERT INTO `product_storage_location` (`id`, `product_id`, `storage_location_id`) VALUES (NULL, $product_id, $sl_id)");
            $stmt->execute();
        }
    }
}
