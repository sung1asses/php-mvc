<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StorageLocation extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT id, title FROM storage_locations');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function findByTitle($title)
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT id, title FROM storage_locations WHERE title = '$title'");
        return $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    }

    public static function getAllWithProducts(){
        $storage_locations = self::getAll();
        $ids = array_map(function ($storage_location){
            return $storage_location['id'];
        }, $storage_locations);
        $ids_string = implode(', ', $ids);

        $db = static::getDB();
        $stmt = $db->query("
            SELECT `products`.*, `product_storage_location`.`storage_location_id` as `pivot_storage_location_id`, `product_storage_location`.`product_id` as `pivot_product_id` 
                FROM `products` 
                    inner join `product_storage_location` 
                    on `products`.`id` = `product_storage_location`.`product_id` 
                where `product_storage_location`.`storage_location_id` in ($ids_string)
        ");
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $storage_locations = array_map(function($storage_location) use ($products){
            $storage_location['products'] = array_filter($products, function($sl) use ($storage_location){
                return $sl['pivot_storage_location_id'] == $storage_location['id'];
            });
            return $storage_location;
        }, $storage_locations);
        return $storage_locations;
    }
}
