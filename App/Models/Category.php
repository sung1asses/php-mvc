<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Category extends \Core\Model
{
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM categories");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getTree()
    {
        $categories = self::getAll();

        return self::generateTree($categories);
    }

    public static function getList()
    {
        $categories = self::getAll();
        $tree =self::generateTree($categories);

        return self::generateList($tree);
    }

    public static function findById($id)
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM categories WHERE id = '$id'");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function findByTitle($title)
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM categories WHERE title = '$title'");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    private static function generateList($categories, $deep = 0){
        $categoriesList = [];
        foreach($categories as $category){
            $category['deep'] = $deep;
            $category_copy = $category;
            unset($category_copy['children']);
            $categoriesList[] = $category_copy;
            if($category['children'])
                $categoriesList = array_merge($categoriesList, self::generateList($category['children'], $deep+1));
        }
        return $categoriesList;
    }

    private static function generateTree($categories){
        $output = array();
        $all = array();
        $dangling = array();
        
        // Initialize arrays
        foreach ($categories as $cateogry) {
            $cateogry['children'] = array();
            $id = $cateogry['id'];
        
            // If this is a top-level node, add it to the output immediately
            if ($cateogry['parent_id'] == null) {
                $all[$id] = $cateogry;
                $output[] =& $all[$id];
        
            // If this isn't a top-level node, we have to process it later
            } else {
                $dangling[$id] = $cateogry; 
            }
        }
        
        // Process all 'dangling' nodes
        while (count($dangling) > 0) {
            foreach($dangling as $cateogry) {
                $id = $cateogry['id'];
                $pid = $cateogry['parent_id'];
        
                // If the parent has already been added to the output, it's
                // safe to add this node too
                if (isset($all[$pid])) {
                    $all[$id] = $cateogry;
                    $all[$pid]['children'][] =& $all[$id]; 
                    unset($dangling[$cateogry['id']]);
                }
            }
        }
        return $output;
    }
}
