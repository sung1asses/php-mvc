<?php

namespace App\Utils;

class Token
{
    protected static $salt = '6fe6f886d2148c5d97e4bfc0741fc218';
    protected static $expire = 3600;

    /**
     * @desc Generate a valid token for 1 hour for the given $userID
     * @param int $userID - the ID of user in database
     * @return string - A valid token for the user
     */
    public static function generate_token($userID)
    {

        $expire = time() + self::$expire; // 1 hour
        $signature = md5($userID . $expire . self::$salt);
        $token = $userID . '.' . $expire . '.' . $signature;

        $encoded = bin2hex($token);

        return $encoded;
    }

    /**
     * @desc Validate a given $token
     * @param string $token - A token generated with generate_token()
     * @return int - Zero for failure or user ID for success
     */
    public static function validate_token($token)
    {

        if (strlen($token) % 2 == 0) {

            $decoded = hex2bin($token);
            if (preg_match('/([0-9]+).([0-9]+).([0-9a-z]{32})/', $decoded, $r)) {
                $userID = $r[1];
                $expire = $r[2];
                $signature = $r[3];

                if ($expire < time()) {
                    return [
                        'status' => 'error',
                        'message' => 'Token is expired'
                    ];
                } else {
                    if ($signature <>  md5($userID . $expire . self::$salt)) {
                        return [
                            'status' => 'error',
                            'message' => 'Invalid signature'
                        ];
                    } else { 
                        return [
                            'status' => 'success',
                            'user_id' => $userID
                        ];
                    }
                }
            } else {
                return [
                    'status' => 'error',
                    'message' => 'Token manipulated'
                ];
            }
        } else {
            return [
                'status' => 'error',
                'message' => 'Token length is wrong'
            ];
        }
    }
}