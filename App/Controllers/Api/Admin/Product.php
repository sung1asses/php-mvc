<?php

namespace App\Controllers\Api\Admin;

use \Core\View;
use App\Utils\Token;

class Product extends \Core\Controller
{
    protected function before(){
        $headers = apache_request_headers();
        if (!isset($headers['Authorization']) || !Token::validate_token($headers['Authorization'])){
            $out = array(
                "status"   => "error",
                "message"   => 'Auth failed',
            );
    
            /* -------------------------------------------------------------------------- */
            /*                                  Response                                  */
            /* -------------------------------------------------------------------------- */
            header('Content-Type: application/json', true, 401);
            header('Access-Control-Allow-Origin: *');
            echo json_encode($out);
            die();
        }
    }

    public function indexAction()
    {
        $out = array(
            "status"   => "success",
            "data"   => [
                'products' => \App\Models\Product::getAllWithRelated()
            ]
        );
        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }

    public function readAction()
    {
        $id = $this->route_params['id'];
        if(isset($id) && $id <> ''){
            $out = array(
                "status"   => "success",
                "data"   => [
                    'product' => \App\Models\Product::findByIdWithRelated($id)
                ]
            );
        }
        else{
            $out = array(
                "status"   => "error",
                "message"   => 'Missing data'
            );
        }

        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }

    public function createAction()
    {
        $data = $this->route_params['data'];
        if($this->validate_data($data, ['title', 'category_id', 'storage_location_ids'])){
            $title = filter_var($data['title'], FILTER_SANITIZE_STRING);
            $category_id = filter_var($data['category_id'], FILTER_SANITIZE_STRING);
            $storage_location_ids = [];
            foreach($data['storage_location_ids'] as $storage_location){
                $storage_location_ids[] = filter_var($storage_location, FILTER_SANITIZE_STRING);
            }

            $product = \App\Models\Product::create([
                'title' => $title,
                'category_id' => $category_id,
                'storage_location_ids' => $storage_location_ids,
            ]);
            $out = array(
                "status"   => "success",
                "data"   => [
                    'product' => $product
                ]
            );
        }
        else{
            $out = array(
                "status"   => "error",
                "message"   => 'Missing data'
            );
        }

        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }

    public function updateAction()
    {
        $id = $this->route_params['id'];
        $data = $this->route_params['data'];
        if(isset($id) && $id <> '' && $this->validate_data($data, ['title', 'category_id', 'storage_location_ids'])){
            $title = filter_var($data['title'], FILTER_SANITIZE_STRING);
            $category_id = filter_var($data['category_id'], FILTER_SANITIZE_STRING);
            $storage_location_ids = [];
            foreach($data['storage_location_ids'] as $storage_location){
                $storage_location_ids[] = filter_var($storage_location, FILTER_SANITIZE_STRING);
            }

            $product = \App\Models\Product::update($id, [
                'title' => $title,
                'category_id' => $category_id,
                'storage_location_ids' => $storage_location_ids,
            ]);
            $out = array(
                "status"   => "success",
                "data"   => [
                    'product' => $product
                ]
            );
        }
        else{
            $out = array(
                "status"   => "error",
                "message"   => 'Missing data'
            );
        }

        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }

    public function deleteAction()
    {
        $id = $this->route_params['id'];
        if(isset($id) && $id <> ''){
            \App\Models\Product::delete($id);
            $out = array(
                "status"   => "success",
                "data"   => []
            );
        }
        else{
            $out = array(
                "status"   => "error",
                "message"   => 'Missing data'
            );
        }

        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }
}
