<?php

namespace App\Controllers\Api\Admin;

use \Core\View;
use App\Utils\Token;

class StorageLocation extends \Core\Controller
{
    protected function before(){
        $headers = apache_request_headers();
        if (!isset($headers['Authorization']) || !Token::validate_token($headers['Authorization'])){
            $out = array(
                "status"   => "error",
                "message"   => 'Auth failed',
            );
    
            /* -------------------------------------------------------------------------- */
            /*                                  Response                                  */
            /* -------------------------------------------------------------------------- */
            header('Content-Type: application/json', true, 401);
            header('Access-Control-Allow-Origin: *');
            echo json_encode($out);
            die();
        }
    }

    public function indexAction()
    {
        $out = array(
            "status"   => "success",
            "data"   => [
                'storage_locations' => \App\Models\StorageLocation::getAll()
            ]
        );
        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }
}
