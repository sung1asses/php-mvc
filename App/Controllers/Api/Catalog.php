<?php

namespace App\Controllers\Api;

use \Core\View;

class Catalog extends \Core\Controller
{
    public function indexAction()
    {
        $out = array(
            "status"   => "success",
            "data"   => [
                'products' => \App\Models\Product::getAllWithRelated()
            ]
        );
        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }
}
