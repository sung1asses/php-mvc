<?php

namespace App\Controllers\Api;

use App\Models\User;
use App\Utils\Token;

class Auth extends \Core\Controller
{
    public function loginAction()
    {   
        $credentials = $this->route_params['data'];
        if (isset($credentials['name']) && $credentials['name'] <> '' && isset($credentials['password']) && $credentials['password'] <> '') {
        
            $name = filter_var($credentials['name'], FILTER_SANITIZE_STRING);
            $password = filter_var($credentials['password'], FILTER_SANITIZE_STRING);

            $user = User::findByName($name);
        
            if ($user) {
        
                if (password_verify($password, $user['password'])) {
                    $out = array(
                        "status"   => "success",
                        "data"   => [
                            'token' => Token::generate_token($user['id'])
                        ]
                    );
                } else {
                    $out = array(
                        "status"   => "error",
                        "message"   => 'Wrong credentials'
                    );
                }
            } else {
                $out = array(
                    "status"   => "error",
                    "message"   => 'Wrong credentials'
                );
            }
        } else {
            $out = array(
                "status"   => "error",
                "message"   => 'Missing data'
            );
        }
        
        /* -------------------------------------------------------------------------- */
        /*                                  Response                                  */
        /* -------------------------------------------------------------------------- */
        header('Content-Type: application/json', true, $out['status'] == 'success' ? 200 : 400);
        header('Access-Control-Allow-Origin: *');
        echo json_encode($out);
    }
}
